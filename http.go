package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/canya-com/canwork-database-client/model"
	"google.golang.org/appengine"
	appenginelog "google.golang.org/appengine/log"
	"google.golang.org/appengine/urlfetch"
)

const (
	// GET : string GET
	GET = "GET"
	// POST : string POST
	POST = "POST"
)

// BadRequest : describes the data structure for wrong requests
type BadRequest struct {
	Message     string              `json:"message,omitempty"`
	Transaction model.Transaction   `json:"transaction,omitempty"`
	Context     context.Context     `json:"-"`
	Writer      http.ResponseWriter `json:"-"`
}

// OnBadRequest : writes a json error http response
func (response *BadRequest) OnBadRequest(httpStatusCode int) {
	appenginelog.Errorf(response.Context, response.Message)
	output, _ := json.Marshal(response)
	response.Writer.WriteHeader(httpStatusCode)
	response.Writer.Write(output)
}

// GetRemoteJSON : gets remote json by url
func GetRemoteJSON(writer http.ResponseWriter, request *http.Request) func(url string, target interface{}) error {
	context := appengine.NewContext(request)
	client := urlfetch.Client(context)
	return func(url string, target interface{}) error {
		appenginelog.Infof(context, "getting http json data from: %s", url)
		response, err := client.Get(url)
		if err != nil {
			return err
		}
		defer response.Body.Close()
		return json.NewDecoder(response.Body).Decode(target)
	}
}

// NewInvalidMethodMessage : returns invalid method message
func NewInvalidMethodMessage(method string, request *http.Request) string {
	return fmt.Sprintf("Incorrect HTTP method: %s, should be a %s request", request.Method, method)
}
