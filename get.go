package main

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"gitlab.com/canya-com/canwork-database-client/model"
	"google.golang.org/appengine"
)

// GetRequest : GET
type GetRequest struct {
	*http.Request
}

// MonitorTransaction : tracks all pending or on_timeout transactions in the database
func (r *GetRequest) MonitorTransaction() func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		defer request.Body.Close()

		context := appengine.NewContext(request)
		var badRequest = BadRequest{
			Context: context,
			Writer:  writer,
		}

		writer.Header().Set("Content-Type", "application/json")

		if request.Method != GET {
			message := NewInvalidMethodMessage(GET, request)
			badRequest.Message = message
			badRequest.OnBadRequest(http.StatusForbidden)
			return
		}

		var tx model.Transaction
		txs := []model.Transaction{}

		databaseClient := tx.Table()
		databaseClient.Where("is_webhook_called = ?", 0).Find(&txs)

		now := time.Now()
		period := "30m"
		dur, _ := time.ParseDuration(period)

		for _, row := range txs {
			ethereumTransaction := Transaction{Transaction: row}

			if ethereumTransaction.LastChecked > 0 && ethereumTransaction.LastChecked-now.Add(-dur).Unix() >= 0 {
				log.Printf("Tx hash [%s] was checked within the last period [%s]. Skipping", ethereumTransaction.Hash, period)
				continue
			}

			if now.Unix() > ethereumTransaction.Timeout {
				log.Printf("Tx hash [%s] has timed-out. Skipping and erasing", ethereumTransaction.Hash)
				ethereumTransaction.OnTimeout(request)
				continue
			}

			err := ethereumTransaction.GetEtherscanTransactionStatusResponse(writer, request)
			if err != nil {
				badRequest.Message = err.Error()
				badRequest.OnBadRequest(http.StatusInternalServerError)
				return
			}
			if ethereumTransaction.EtherscanTransactionStatusResponse.Status == "1" {
				log.Printf("Tx hash known to Etherscan: [%s]", ethereumTransaction.Hash)
				log.Printf("Ethereum TX Status: [%s]", ethereumTransaction.EtherscanTransactionStatusResponse.Message)
				err := ethereumTransaction.GetEtherscanTransactionReceiptStatusResponse(writer, request)
				if err != nil {
					badRequest.Message = err.Error()
					badRequest.OnBadRequest(http.StatusInternalServerError)
					return
				}

				log.Printf("Tx receipt status: [%s]", ethereumTransaction.EtherscanTransactionReceiptStatusResponse.Result.Status)

				if ethereumTransaction.EtherscanTransactionReceiptStatusResponse.Result.Status == "" {
					ethereumTransaction.OnStatusPending(request)
				} else if ethereumTransaction.EtherscanTransactionReceiptStatusResponse.Result.Status == "1" {
					ethereumTransaction.OnStatusSuccess(request)
				} else {
					ethereumTransaction.OnStatusFailed(request)
				}

				updateQuery := model.Transaction{
					MonitorCount: ethereumTransaction.MonitorCount + 1,
					LastChecked:  time.Now().Unix(),
				}
				ethereumTransaction.Transaction.Table().Where("hash = ?", ethereumTransaction.Hash).Updates(updateQuery)
			}
		}

		output, err := json.Marshal(txs)
		if err != nil {
			message := err.Error()
			badRequest.Message = message
			badRequest.OnBadRequest(http.StatusInternalServerError)
			return
		}

		writer.Write(output)
	}
}

// TransactionDetails : GET tx by id
func (r *GetRequest) TransactionDetails() func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		defer request.Body.Close()

		var badRequest = BadRequest{
			Context: appengine.NewContext(request),
			Writer:  writer,
		}

		writer.Header().Set("Content-Type", "application/json")

		if request.Method != GET {
			message := NewInvalidMethodMessage(GET, request)
			badRequest.Message = message
			badRequest.OnBadRequest(http.StatusForbidden)
			return
		}

		hash := request.URL.Query().Get("hash")

		query := model.Transaction{
			Hash: hash,
		}

		var tx model.Transaction

		databaseClient := query.GetRecordByHash(&tx)
		if databaseClient.RecordNotFound() {
			message := "No records on transaction table"
			badRequest.Message = message
			badRequest.OnBadRequest(http.StatusInternalServerError)
			return
		}

		output, err := json.Marshal(tx)
		if err != nil {
			message := "GET GetTransaction failed to Marshal transaction"
			badRequest.Message = message
			badRequest.OnBadRequest(http.StatusInternalServerError)
			return
		}

		writer.Write(output)
	}
}
