package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/jinzhu/gorm"
	database "gitlab.com/canya-com/canwork-database-client"
	"google.golang.org/appengine"
)

var (
	// DatabaseClient : global gorm.DB instance
	DatabaseClient *gorm.DB
	// DefaultTimeout : 1 day
	DefaultTimeout = 1
	// EtherscanURI : etherscan api url
	EtherscanURI string
	// EtherscanAPIKey : n
	EtherscanAPIKey string
	// EthereumNetwork : n
	EthereumNetwork string
)

func init() {
	setEtherscanEnvironment()
	makeDatabaseConnection()

	GET := GetRequest{}
	POST := PostRequest{}

	http.HandleFunc("/tx/details", GET.TransactionDetails())
	http.HandleFunc("/tx/monitor", GET.MonitorTransaction())
	http.HandleFunc("/tx/store", POST.StoreTransaction())
}

func main() {
	defer DatabaseClient.Close()
	appengine.Main()
}

func makeDatabaseConnection() {
	var err error
	dsn := makeDsnString()
	DatabaseClient, err = database.NewDatabaseClient("mysql", dsn)
	if err != nil {
		log.Fatalf(err.Error())
	}
}

func setEtherscanEnvironment() {
	EthereumNetwork = GetEnv("ETHEREUM_NETWORK", "mainnet")
	EtherscanAPIKey = GetEnv("ETHERSCAN_API_KEY", "")
	EtherscanURI = GetEnv("ETHERSCAN_URI", "")
}

func makeDsnString() string {
	connection := fmt.Sprintf("%s", GetEnv("DB_CONNECTION", ""))
	message := fmt.Sprintf("Setting DB_CONNECTION: [%s]", connection)
	log.Printf(message)
	return connection
}
