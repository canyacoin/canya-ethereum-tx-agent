# Canya Ethereum TX API

Store and monitor Ethereum transactions. Call a webhook on complete or timedout transactions.

## Usage

Call the public endpoints:

### Public endpoints

#### `POST /api/v1/tx/store`

Accepts the following JSON payload:

```json
{
	"hash": "0xd182288466141a5cb8b97e913d43a51adbf3d9fdcecaf159e0f08cc7dae6e30e",
	"from": "0xd5be931bd4e18007a09e6b20e25e8491e770e775",
	"webhookOnSuccess": "http://localhost:1883/tx/details?hash=0xd182288466141a5cb8b97e913d43a51adbf3d9fdcecaf159e0f08cc7dae6e30e",
	"webhookOnTimeout": "http://localhost:1883/tx/details?hash=0xd182288466141a5cb8b97e913d43a51adbf3d9fdcecaf159e0f08cc7dae6e30e"
}
```

Optional payload `key,values`: 

```
{
    "timeout": {unix timestamp}, // default 1 day,
    "network": {string}, // defaults to "mainnet"
}
```

#### `GET /api/v1/tx/details?hash={tx-hash}`

Gets the details of a given transaction hash, if stored.