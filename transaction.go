package main

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/canya-com/canwork-database-client/model"
	"google.golang.org/appengine"
	appenginelog "google.golang.org/appengine/log"
	"google.golang.org/appengine/urlfetch"
)

// Transaction : a Transaction model with EtherscanStatus responses
type Transaction struct {
	model.Transaction
	*EtherscanTransactionStatusResponse
	*EtherscanTransactionReceiptStatusResponse
}

// EtherscanTransactionStatusResponse is the response structure returned from calling etherscan's "getstatus" method
type EtherscanTransactionStatusResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	Result  struct {
		IsError        string `json:"isError"`
		ErrDescription string `json:"errDescription"`
	} `json:"result"`
}

// EtherscanTransactionReceiptStatusResponse is the response structure returned from calling etherscan's "gettxreceiptstatus" method
type EtherscanTransactionReceiptStatusResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	Result  struct {
		Status string `json:"status"`
	} `json:"result"`
}

// GetEtherscanTransactionStatusResponse : n
func (tx *Transaction) GetEtherscanTransactionStatusResponse(writer http.ResponseWriter, request *http.Request) error {
	context := appengine.NewContext(request)
	URI := fmt.Sprintf("%s/api?module=transaction&action=getstatus&txhash=%s&apikey=%s", EtherscanURI, tx.Hash, EtherscanAPIKey)
	err := GetRemoteJSON(writer, request)(URI, &tx.EtherscanTransactionStatusResponse)
	if err != nil {
		appenginelog.Errorf(context, "error calling etherscan (getstatus) API: %v", err)
	}
	return err
}

// GetEtherscanTransactionReceiptStatusResponse : n
func (tx *Transaction) GetEtherscanTransactionReceiptStatusResponse(writer http.ResponseWriter, request *http.Request) error {
	context := appengine.NewContext(request)
	URI := fmt.Sprintf("%s/api?module=transaction&action=gettxreceiptstatus&txhash=%s&apikey=%s", EtherscanURI, tx.Hash, EtherscanAPIKey)
	err := GetRemoteJSON(writer, request)(URI, &tx.EtherscanTransactionReceiptStatusResponse)
	if err != nil {
		appenginelog.Errorf(context, "error calling etherscan (gettxreceiptstatus) API: %v", err)
	}
	return err
}

// OnStatusSuccess : changes tx status in database and calls webhook_on_success
func (tx *Transaction) OnStatusSuccess(request *http.Request) {
	context := appengine.NewContext(request)
	url := tx.WebhookOnSuccess
	log.Printf("Calling webhook_on_success: [%s]", url)
	client := urlfetch.Client(context)
	_, err := client.Get(url)
	if err != nil {
		message := fmt.Sprintf("ERROR while calling webhook_on_success [%s] - [%s]", url, err.Error())
		log.Printf(message)
		appenginelog.Errorf(context, message)
	}
	status := tx.StatusSuccess()
	log.Printf("tx status: [%s]", status)
	tx.Transaction.Table().Where("hash = ?", tx.Hash).Update("is_webhook_called", 1).Update("status", status)
}

// OnStatusPending : changes tx status in database
func (tx *Transaction) OnStatusPending(request *http.Request) {
	status := tx.StatusPending()
	log.Printf("tx status: [%s]", status)
	tx.Transaction.Table().Where("hash = ?", tx.Hash).Update("status", status)
}

// OnStatusFailed : changes tx status in database
func (tx *Transaction) OnStatusFailed(request *http.Request) {
	status := tx.StatusFailed()
	log.Printf("tx status: [%s]", status)
	tx.Transaction.Table().Where("hash = ?", tx.Hash).Update("status", status)
}

// OnTimeout : changes tx status in database
func (tx *Transaction) OnTimeout(request *http.Request) {
	context := appengine.NewContext(request)
	url := tx.WebhookOnTimeout
	log.Printf("Calling webhook_on_timeout: [%s]", url)
	client := urlfetch.Client(context)
	_, err := client.Get(url)
	if err != nil {
		message := fmt.Sprintf("ERROR while calling webhook_on_timeout [%s] - [%s]", url, err.Error())
		log.Printf(message)
		appenginelog.Errorf(context, message)
	}
	tx.Transaction.Table().Where("hash = ?", tx.Hash).Delete(tx)
}
